package by.loadcrew.restfulback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestfulbackApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestfulbackApplication.class, args);
    }

}
